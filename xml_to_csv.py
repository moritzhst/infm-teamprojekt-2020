import glob
import pandas as pd
import xml.etree.ElementTree as ET

X_SIZE = 1280
Y_SIZE = 960


def main():
    for folder in ['train', 'test']:
        image_path = 'images/' + folder
        xml_df = xml_to_csv(image_path)
        xml_df.to_csv((folder + '_labels.csv'), index=False)
        print('Successfully converted xml to csv.')


def correct_coords(coord, axis):
    coord = int(coord)
    if coord < 0:
        return 0
    if axis == 'x':
        return X_SIZE if coord > X_SIZE else coord
    else:
        return Y_SIZE if coord > Y_SIZE else coord


def xml_to_csv(path):
    xml_list = []
    for xml_file in glob.glob(path + '/*.xml'):
        tree = ET.parse(xml_file)
        root = tree.getroot()
        for member in root.findall('object'):
            try:
                label = member.find('name').text.strip()

                if label != "car" and label != "pedestrian" and label != "bicycle":
                    continue;

            except:
                label = "unknown"

            x_points = []
            y_points = []
            polygons = member.findall('polygon')

            for polygon in polygons:
                points = polygon.findall('pt')
                for point in points:
                    x_points.append(correct_coords(point[0].text, 'x'))
                    y_points.append(correct_coords(point[1].text, 'y'))

            if label != 'unknown':
                value = (root.find('filename').text.strip(), X_SIZE, Y_SIZE, label.strip(), min(x_points), min(y_points), max(x_points),
                     max(y_points))

                xml_list.append(value)

    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(xml_list, columns=column_name)
    return xml_df


main()
